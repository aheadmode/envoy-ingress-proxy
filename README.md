# envoy-ingress-proxy

[![Envoy Logo](https://gitlab.com/aheadmode/envoy-ingress-proxy/-/raw/master/envoy.png)](https://www.envoyproxy.io/)

Cloud-native high-performance edge/middle/service proxy.

This repository provides a minimal [Envoy Proxy](https://www.envoyproxy.io/) container image, including custom http filter extensions.

### HTTP filter extensions

- **language**

    This is a filter which determines the language the client is able to understand based on
    the web interface language parameter (hl=), a language cookie or the [Accept-Language](https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS)
    request HTTP header advertisement.

      * This filter should be configured with the name envoy.filters.http.language.

    ```yaml
    name: envoy.filters.http.language
    config:
      default_language: "en"
      # List of supported languages
      languages: ["en", "de", "dk", "es", "fr", "it", "pt", "jp", "ko", "zh", "zh-tw"]
    ```
    
    ##### HTTP headers (created)

    - x-language
    
### Prerequisites

- [Bazel](https://bazel.build/)
- [cmake](https://cmake.org/)
- [Ninja](https://ninja-build.org/)
- [clang](https://clang.llvm.org/)
- [libc++](https://libcxx.llvm.org/)
- [lld](https://lld.llvm.org/)

## Getting started

### Building the source code

It is recommended to use the prebuilt Clang+LLVM package from [LLVM official site](http://releases.llvm.org/download.html).<br>
Extract the tar.xz and setup your environment

```
$ setup_clang.sh <PATH_TO_CLANG_LLVM_DIR>
```

This will create a `clang.bazelrc` file in the root directory automatically.

Build the static binary by linking against libc++

```sh
$ bazel build --config=libc++ :envoy
```

Run the binary locally 

```sh
$ ./bazel-bin/envoy --local-address-ip-version v4 --config-path config.yaml
```

##### Querying the build graph

Bazel constructs a graph out of build targets, which provides lots of useful information.<br>
Using the graphviz optional dependency, generate a program `dot` with [bazel query](https://docs.bazel.build/versions/master/query-how-to.html)

```sh
$ bazel query --output=graph '//...' | dot -Tpng > graph.png
```

### Deploying

[OCI Image Format](https://github.com/opencontainers/image-spec) is the default container format.

Build the container image

```sh
$ bazel build --config=libc++ :envoy_container_image.tar
```

Load the image locally

```sh
$ podman load < bazel-bin/envoy_container_image.tar
```

List the image

```sh
$ podman images
REPOSITORY             TAG                          IMAGE ID       CREATED          SIZE
localhost/bazel        envoy_container_image        d4bfad90e657   11 seconds ago   182 MB
```

Run the container

```sh
$ podman run -d --name envoy-ingress-proxy --network host --rm localhost/bazel:envoy_container_image
```

### Testing

Run the integration test

```sh
$ bazel test //src/filters/http/language:language_filter_integration_test
```

Run the echo2 integration test

```sh
$ bazel test //:echo2_integration_test
```

Run the regular Envoy tests from this project

```sh
$ bazel test @envoy//test/...
```
