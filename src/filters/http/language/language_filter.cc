#include <string>

#include "language_filter.h"

#include "unicode/utypes.h"
#include "unicode/locid.h"

#include "common/common/empty_string.h"
#include "common/common/logger.h"
#include "common/common/utility.h"
#include "common/http/utility.h"

namespace Envoy {
namespace Http {

HttpLanguageDecoderFilterConfig::HttpLanguageDecoderFilterConfig(
    const envoy::filters::http::language::Decoder& proto_config)
    : default_language_(proto_config.default_language().data()) {
  for (const auto& header : proto_config.languages()) {
    languages_.push_back(icu::Locale(header.data()));
  }
}

HttpLanguageDecoderFilter::HttpLanguageDecoderFilter(HttpLanguageDecoderFilterConfigSharedPtr config)
    : config_(config) {
    UErrorCode errorCode = U_ZERO_ERROR;

    matcher_ = std::make_shared<icu::LocaleMatcher>(icu::LocaleMatcher::Builder()
        .setSupportedLocales(config_->languages().begin(), config_->languages().end())
        .setDefaultLocale(&config_->default_language())
        .build(errorCode));
}

HttpLanguageDecoderFilter::~HttpLanguageDecoderFilter() {}

void HttpLanguageDecoderFilter::onDestroy() {}

FilterHeadersStatus HttpLanguageDecoderFilter::decodeHeaders(RequestHeaderMap& headers, bool) {
  // Check web interface language param first
  Http::Utility::QueryParams params = Http::Utility::parseQueryString(headers.Path()->value().getStringView());

  if (!params.empty()) {
    absl::optional<std::string> param_value = (params.find(language_param_) != params.end()) ?
      absl::optional<std::string>{params.at(language_param_)} :
      absl::nullopt;

    if (param_value.has_value()) {
      const std::string& value = param_value.value();
      icu::Locale locale = icu::Locale(value.data());
      std::string language_tag = matchValue(locale);

      if (!language_tag.empty()) {
        headers.addCopy(Language, language_tag);
        return FilterHeadersStatus::Continue;
      }
    }
  }

  // Check cookie value
  const std::string value = Utility::parseCookieValue(headers, language_cookie_key_);
  if (!value.empty()) {
    icu::Locale locale = icu::Locale(value.data());
    std::string language_tag = matchValue(locale);

    if (!language_tag.empty()) {
      headers.addCopy(Language, language_tag);
      return FilterHeadersStatus::Continue;
    }
  }

  //Check accept language header
  const Http::HeaderEntry* entry = headers.get(AcceptLanguage);
  if (entry != nullptr) {
    absl::string_view value_str = entry->value().getStringView();

    if (!value_str.empty()) {
      std::string value = std::string(value_str);

      UErrorCode errorCode = U_ZERO_ERROR;
      const icu::Locale *result = matcher_->getBestMatchForListString(value, errorCode);

      if (U_SUCCESS(errorCode)) {
        std::string language_tag = result->toLanguageTag<std::string>(errorCode).data();

        if (U_SUCCESS(errorCode)) {

          if (!language_tag.empty()) {
            headers.addCopy(Language, language_tag);
            return FilterHeadersStatus::Continue;
          }
        }
      }
    }
  }

  // Default language fallback
  headers.addCopy(Language, config_->default_language().getLanguage());
  return FilterHeadersStatus::Continue;
}

FilterDataStatus HttpLanguageDecoderFilter::decodeData(Buffer::Instance&, bool) {
  return FilterDataStatus::Continue;
}

FilterTrailersStatus HttpLanguageDecoderFilter::decodeTrailers(RequestTrailerMap&) {
  return FilterTrailersStatus::Continue;
}

void HttpLanguageDecoderFilter::setDecoderFilterCallbacks(StreamDecoderFilterCallbacks& callbacks) {
  decoder_callbacks_ = &callbacks;
}

std::string HttpLanguageDecoderFilter::matchValue(icu::Locale& locale) {
  UErrorCode errorCode = U_ZERO_ERROR;
  const icu::Locale *result = matcher_->getBestMatch(locale, errorCode);

  if (U_SUCCESS(errorCode)) {
    auto language_tag = result->toLanguageTag<std::string>(errorCode).data();

    if (U_SUCCESS(errorCode)) {
      return language_tag;
    } else {
      //ENVOY_LOG(error, "ICU error code toLanguageTag: {}", errorCode);
    }
  } else {
    //ENVOY_LOG(error, "ICU error code getBestMatch: {}", errorCode);
  }

  return EMPTY_STRING;
}

} // namespace Http
} // namespace Envoy
