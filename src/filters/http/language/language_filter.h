#pragma once

#include <string>

#include "unicode/localematcher.h"

#include "envoy/server/filter_config.h"

#include "src/filters/http/language/language.pb.h"

namespace Envoy {
namespace Http {

class HttpLanguageDecoderFilterConfig {
public:
  HttpLanguageDecoderFilterConfig(const envoy::filters::http::language::Decoder& proto_config);

  const icu::Locale& default_language() const { return default_language_; }
  const std::vector<icu::Locale>& languages() const { return languages_; }

private:
  const icu::Locale default_language_;
  std::vector<icu::Locale> languages_;
};

typedef std::shared_ptr<HttpLanguageDecoderFilterConfig> HttpLanguageDecoderFilterConfigSharedPtr;

class HttpLanguageDecoderFilter : public StreamDecoderFilter {
public:
  HttpLanguageDecoderFilter(HttpLanguageDecoderFilterConfigSharedPtr);
  ~HttpLanguageDecoderFilter();

  const LowerCaseString Language{"x-language"};
  const LowerCaseString AcceptLanguage{"accept-language"};
  
  // Http::StreamFilterBase
  void onDestroy() override;

  // Http::StreamDecoderFilter
  FilterHeadersStatus decodeHeaders(RequestHeaderMap&, bool) override;
  FilterDataStatus decodeData(Buffer::Instance&, bool) override;
  FilterTrailersStatus decodeTrailers(RequestTrailerMap&) override;
  void setDecoderFilterCallbacks(StreamDecoderFilterCallbacks&) override;

private:
  const HttpLanguageDecoderFilterConfigSharedPtr config_;
  StreamDecoderFilterCallbacks* decoder_callbacks_;

  const std::string language_cookie_key_ = "language";
  const std::string language_param_ = "hl";

  std::shared_ptr<icu::LocaleMatcher> matcher_;

  std::string matchValue(icu::Locale&);
};

} // namespace Http
} // namespace Envoy
