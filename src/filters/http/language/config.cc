#include <string>

#include "language_filter.h"
#include "src/filters/http/language/language.pb.h"
#include "src/filters/http/language/language.pb.validate.h"

#include "envoy/registry/registry.h"

namespace Envoy {
namespace Server {
namespace Configuration {

class HttpLanguageDecoderFilterConfig : public NamedHttpFilterConfigFactory {
public:
  Http::FilterFactoryCb createFilterFactoryFromProto(const Protobuf::Message& proto_config,
                                                     const std::string&,
                                                     FactoryContext& context) override {

    return createFilter(Envoy::MessageUtil::downcastAndValidate<const envoy::filters::http::language::Decoder&>(
                            proto_config, context.messageValidationVisitor()),
                        context);
  }

  /**
   *  Return the Protobuf Message that represents your config incase you have config proto
   */
  ProtobufTypes::MessagePtr createEmptyConfigProto() override {
    return ProtobufTypes::MessagePtr{new envoy::filters::http::language::Decoder()};
  }

  std::string name() const override { return "envoy.filters.http.language"; }

private:
  Http::FilterFactoryCb createFilter(const envoy::filters::http::language::Decoder& proto_config, FactoryContext&) {
    Http::HttpLanguageDecoderFilterConfigSharedPtr config =
        std::make_shared<Http::HttpLanguageDecoderFilterConfig>(
            Http::HttpLanguageDecoderFilterConfig(proto_config));

    return [config](Http::FilterChainFactoryCallbacks& callbacks) -> void {
      auto filter = new Http::HttpLanguageDecoderFilter(config);
      callbacks.addStreamDecoderFilter(Http::StreamDecoderFilterSharedPtr{filter});
    };
  }
};

/**
 * Static registration for this language filter. @see RegisterFactory.
 */
static Registry::RegisterFactory<HttpLanguageDecoderFilterConfig, NamedHttpFilterConfigFactory>
    register_;

} // namespace Configuration
} // namespace Server
} // namespace Envoy
