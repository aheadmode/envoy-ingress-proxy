#include "test/integration/http_integration.h"

namespace Envoy {
class HttpFilterLanguageIntegrationTest : public HttpIntegrationTest,
                                        public testing::TestWithParam<Network::Address::IpVersion> {
public:
  HttpFilterLanguageIntegrationTest()
      : HttpIntegrationTest(Http::CodecClient::Type::HTTP1, GetParam(), realTime()) {}
  /**
   * Initializer for an individual integration test.
   */
  void SetUp() override { initialize(); }

  void initialize() override {
    config_helper_.addFilter("{ name: envoy.filters.http.language, config: { default_language: en, languages: [en, de, fr] } }");
    HttpIntegrationTest::initialize();
  }
};

INSTANTIATE_TEST_SUITE_P(IpVersions, HttpFilterLanguageIntegrationTest,
                        testing::ValuesIn(TestEnvironment::getIpVersionsForTest()));

TEST_P(HttpFilterLanguageIntegrationTest, Test1) {
  Http::TestRequestHeaderMapImpl headers{
      {":method", "GET"}, {":path", "/"}, {":authority", "host"}};

  IntegrationCodecClientPtr codec_client;
  FakeHttpConnectionPtr fake_upstream_connection;
  FakeStreamPtr request_stream;

  codec_client = makeHttpConnection(lookupPort("http"));
  auto response = codec_client->makeHeaderOnlyRequest(headers);
  ASSERT_TRUE(fake_upstreams_[0]->waitForHttpConnection(*dispatcher_, fake_upstream_connection));
  ASSERT_TRUE(fake_upstream_connection->waitForNewStream(*dispatcher_, request_stream));
  ASSERT_TRUE(request_stream->waitForEndStream(*dispatcher_));
  response->waitForEndStream();

  EXPECT_EQ("en",
               request_stream->headers().get(Http::LowerCaseString("X-Language"))->value().getStringView());

  codec_client->close();
}

TEST_P(HttpFilterLanguageIntegrationTest, Test2) {
  Http::TestRequestHeaderMapImpl headers{
      {":method", "GET"}, {":path", "/test?hl=de"}, {":authority", "host"}};

  IntegrationCodecClientPtr codec_client;
  FakeHttpConnectionPtr fake_upstream_connection;
  FakeStreamPtr request_stream;

  codec_client = makeHttpConnection(lookupPort("http"));
  auto response = codec_client->makeHeaderOnlyRequest(headers);
  ASSERT_TRUE(fake_upstreams_[0]->waitForHttpConnection(*dispatcher_, fake_upstream_connection));
  ASSERT_TRUE(fake_upstream_connection->waitForNewStream(*dispatcher_, request_stream));
  ASSERT_TRUE(request_stream->waitForEndStream(*dispatcher_));
  response->waitForEndStream();

  EXPECT_EQ("de",
               request_stream->headers().get(Http::LowerCaseString("X-Language"))->value().getStringView());

  codec_client->close();
}

TEST_P(HttpFilterLanguageIntegrationTest, Test3) {
  Http::TestRequestHeaderMapImpl headers{
      {":method", "GET"}, {":path", "/test?hl=fr"}, {":authority", "host"}};

  IntegrationCodecClientPtr codec_client;
  FakeHttpConnectionPtr fake_upstream_connection;
  FakeStreamPtr request_stream;

  codec_client = makeHttpConnection(lookupPort("http"));
  auto response = codec_client->makeHeaderOnlyRequest(headers);
  ASSERT_TRUE(fake_upstreams_[0]->waitForHttpConnection(*dispatcher_, fake_upstream_connection));
  ASSERT_TRUE(fake_upstream_connection->waitForNewStream(*dispatcher_, request_stream));
  ASSERT_TRUE(request_stream->waitForEndStream(*dispatcher_));
  response->waitForEndStream();

  EXPECT_EQ("fr",
               request_stream->headers().get(Http::LowerCaseString("X-Language"))->value().getStringView());

  codec_client->close();
}

TEST_P(HttpFilterLanguageIntegrationTest, Test4) {
  Http::TestRequestHeaderMapImpl headers{
      {":method", "GET"}, {":path", "/test?hl=foo"}, {":authority", "host"}};

  IntegrationCodecClientPtr codec_client;
  FakeHttpConnectionPtr fake_upstream_connection;
  FakeStreamPtr request_stream;

  codec_client = makeHttpConnection(lookupPort("http"));
  auto response = codec_client->makeHeaderOnlyRequest(headers);
  ASSERT_TRUE(fake_upstreams_[0]->waitForHttpConnection(*dispatcher_, fake_upstream_connection));
  ASSERT_TRUE(fake_upstream_connection->waitForNewStream(*dispatcher_, request_stream));
  ASSERT_TRUE(request_stream->waitForEndStream(*dispatcher_));
  response->waitForEndStream();

  EXPECT_EQ("en",
               request_stream->headers().get(Http::LowerCaseString("X-Language"))->value().getStringView());

  codec_client->close();
}

TEST_P(HttpFilterLanguageIntegrationTest, Test5) {
  Http::TestRequestHeaderMapImpl headers{
      {":method", "GET"}, {":path", "/test?hl"}, {":authority", "host"}};

  IntegrationCodecClientPtr codec_client;
  FakeHttpConnectionPtr fake_upstream_connection;
  FakeStreamPtr request_stream;

  codec_client = makeHttpConnection(lookupPort("http"));
  auto response = codec_client->makeHeaderOnlyRequest(headers);
  ASSERT_TRUE(fake_upstreams_[0]->waitForHttpConnection(*dispatcher_, fake_upstream_connection));
  ASSERT_TRUE(fake_upstream_connection->waitForNewStream(*dispatcher_, request_stream));
  ASSERT_TRUE(request_stream->waitForEndStream(*dispatcher_));
  response->waitForEndStream();

  EXPECT_EQ("en",
               request_stream->headers().get(Http::LowerCaseString("X-Language"))->value().getStringView());

  codec_client->close();
}
} // namespace Envoy
